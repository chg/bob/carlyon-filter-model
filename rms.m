function rootmeansquare=rms(array)

rootmeansquare=norm(array)/sqrt(length(array));