%This file gives an example implementation of the model proposed by Carlyon
%et al (Hearing Research, 2005) for predicting thresholds for electrical
%stimulation of a cochlear implant. 
%(R. P. Carlyon, A. van Wieringen, J. M. Deeks, C.J.  Long, J.  Lyzenga,
%and J. Wouters (in press). �Effect of inter-phase gap on the sensitivity of cochlear implant users to electrical stimulation�. Hearing Research).
%
%It calls the function bob_filter_IPG_func_Window2 which generates a pulse
%train, passes it through a linear lowpass filter, and calculates the rms
%output over a Hanning window. The function is quite flexible as to which
%pulse trains can be generated, and can be easily modified to generate
%other stimuli (e.g. sinusoids). Further details can be obtained by typing
%help bob_filter_IPG_func_Window2
%
%The present example is for a 100pps train of biphasic pulses in which the
%leading polarity of each pulse is positive. Thresholds are calculated as a
%function of inter-phase gap, similar to experimnt 2 of Carlyon et al
%(2005)
%
% No responsibility is accepted for how you use these routines. To state
% the bleeding obvious: take care when presenting a novel pattern of
% electrical stimulation to an implant user.
%
% Bob Carlyon, April 2005


sampfreq=100000;
duration=100;
puldur=0.1;% bob_filter_IPG_func_Window2 can specify different durations for the two phases of a biphasic pulse. We use a common duration here

IPG=[8 100 300 600 900 1900 2900];
for i=1:length(IPG)
    i
    [output(i),gain,sig]=bob_filter_IPG_func_Window2(sampfreq,1,1,puldur,puldur,10,IPG(i),duration,20);
    clear sig;% in case different signals differ in length. This can happen because the above function generates an integer number of periods
end    

 
maxout=max(output); % calculate maximum model output (this corresponds to minimum predicted threshold)

threshremin=maxout-output;


figure(1);plot(IPG,threshremin)
xlabel('inter-phase gap (us)');ylabel('Predicted Threshold, dB re min'); 

    
%     num_pts=duration*0.001*sampfreq;
%     t=([1:num_pts])/sampfreq;
%     plot(t,sig);