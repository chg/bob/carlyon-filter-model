Software for predicting thresholds obtained with pulsatile electrical 
stimulation


Carlyon et al ( 2005) described a simple model which predicts cochlear implant 
users� thresholds for electrical pulse trains. We have found that it successfully 
captures the effects of inter-phase gap, phase duration, and pulse rate over a wide 
range, and works both for symmetric pulses and for �pseudomonophasic� pulse 
trains. The model works by by passing the stimulus through a low-pass filter 
designed to capture the effects of frequency on detection thresholds for 
sinusoidal electrical stimulation. The software generates the pulse train for you, 
but can easily be modified to generate sine waves or indeed any analogue 
waveform. It is written in MATLAB and should run on any platform supporting 
that language, although we have only tested it on Windows XP.


If you use the software, please cite the following publication, which contains full 
details of how the model works: 

R. P. Carlyon, A. van Wieringen, J. M. Deeks, C.J.  Long, J.  Lyzenga, 
and J. Wouters (2005). �Effect of inter-phase gap on the sensitivity of 
cochlear implant users to electrical stimulation�. Hearing Research 205, 210-224
The software can be downloaded as a zip file. We hope you will find it useful but 
neither the authors nor their employers accept any responsibility for the 
consequences of its use. 

If you use the software, please cite the following publication, which contains full 
details of how the model works: 

R. P. Carlyon, A. van Wieringen, J. M. Deeks, C.J.  Long, J.  Lyzenga, 
and J. Wouters (2005). �Effect of inter-phase gap on the sensitivity of 
cochlear implant users to electrical stimulation�. Hearing Research 205, 210-224

*****
The Software is the result of a collaboration between the MRC and the Chancellor Masters and Scholars of the University of Cambridge (the �University�)

"The Software shall be used for non-commercial research purposes only.  The USER will not reverse engineer, manufacture, sell or sublicense for manufacture and sale upon a commercial basis the Software, incorporate it into other software or products or use the Software other than herein expressly specified.
 
The USER agrees that it will not translate, alter, decompile, disassemble, reverse engineer, reverse compile, attempt to derive, or reproduce source code from the Software. The USER also agrees that it will not remove any copyright or other proprietary or product identification notices from the Software.
 
The Software is provided without warranty of merchantability or fitness for a particular purpose or any other warranty, express or implied, and without any representation or warranty that the use or supply of the Software will not infringe any patent, copyright, trademark or other right.
 
In no event shall MRC, the University or their staff and students be liable for any use by the USER of the Software.
 
The supply of the Software does not grant the USER any title or interest in and to the Software, the related source code, and any and all associated patents, copyrights, and other intellectual property rights.
 
MRC, the University and their staff and students reserve the right to revise the Software and to make changes therein from time to time without obligation to notify any person or organisation of such revision or changes.
 
While MRC and the University will make every effort to ensure the accuracy of Software and the data contained therein, however neither MRC nor its employees or agents may be held responsible for errors, omissions or other inaccuracies or any consequences thereof."
***** 

License: CC-BY-NC-SA