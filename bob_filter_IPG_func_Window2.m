function [output,gain,sig]=bob_filter_IPG_func_Window2(fs,startpolarity,biphasic,puldur1,puldur2,period,ipg,dur,ThreshWindow);
%Generates a pulse train and passes it through linear filter model
%described by Carlyon et al, 2005
%(R. P. Carlyon, A. van Wieringen, J. M. Deeks, C.J.  Long, J.  Lyzenga, and J. Wouters (2005). �Effect of inter-phase gap on the sensitivity of cochlear implant users to electrical stimulation�. Hearing Research 205 p210-224).
%
%function [output,gain,sig]=bob_filter_IPG_func_Window2(fs,startpolarity,biphasic,puldur1,puldur2,period,ipg,dur,ThreshWindow);
%fs=sampling rate - enter 0 for default of 100000
%startpolarity: -1=initial negative,0=alternate,1=initial positive
%biphasic: 0=monophasic 1 = biphasic
%puldur1: duration of 1st phase of the pulse in ms
%puldur2: duration of 1st phase of the pulse in ms
%period: period of waveform in ms
%ipg: inter-phase gap in microseconds
%dur: duration of total waveform in ms
%ThreshWindow: total duration of hanning window over which to calculate
%output. Carlyon et al (2005) used 20 ms
%The function was written by Bob Carlyon and uses the filter implementation by
%Johannes Lyzenga. Neither of them accept any responsibility for the
%consequences of you using these routines. The same goes for their
%employers, friends, relatives etc. 

clear output;clear gain;clear c;clear sig;clear threshremax;

close all;
if fs==0, fs=100000, end;
x=johannes_ir1(fs,2048,100,200); y=fft(x);%x is the filter impulse response!


numperiods=round(dur/period);
newdur=numperiods*period; % forces duration to be integer number of periods
numsamps=round(fs*0.001*newdur);

periodsamps=round(fs*0.001*period);
ipg=ipg/1000;%convert ipg to milliseconds
pulsamps1=round(fs*0.001*puldur1);
pulsamps2=round(fs*0.001*puldur2);
t=[1:numsamps]/fs;
if startpolarity==-1
    polarity=-1;
else
    polarity=1;
end;

    ampratio=puldur1/puldur2;
    gapsamps=round(fs*0.001*ipg);
    sig(1:pulsamps1)=polarity;
    sig(pulsamps1+1:pulsamps1+gapsamps)=0;
    sig(pulsamps1+gapsamps+1:pulsamps1+gapsamps+pulsamps2)=polarity*ampratio*biphasic*-1;
   sig(pulsamps1+pulsamps2+gapsamps+1:periodsamps)=0;
 
    if startpolarity==0
        for i=2:2:numperiods-1
        sig(1+i*periodsamps:(i+1)*periodsamps)=sig(1:periodsamps);
        end;
        for i=1:2:numperiods-1
        sig(1+i*periodsamps:(i+1)*periodsamps)=-1*sig(1:periodsamps);
        end;   
    else
        for i=1:numperiods-1    
        sig(1+i*periodsamps:(i+1)*periodsamps)=sig(1:periodsamps);
        end;
    end;   
%     figure(1);
%     plot(sig); plot the pulse train
    c=conv(sig,x);
    gain=20*log10(rms(c)/rms(sig));

  if(ThreshWindow>0.0)
        %try not to sample 1st period, which can be different to all others
        if (numperiods >=3) startperiod=4; else startperiod=numperiods; end;
        ThreshPoints=ThreshWindow*fs*0.001;
        window=hanning(ThreshPoints)';
        count=0;output=-9999;
        %need to correct for group delay of filter which is half the length
        %of its impulse response (P. Lynn: An introduction the analysis &
        %processing of signals)
        GrpDelPts=round(length(x)/2);
        for WinStartPoint=(periodsamps*(startperiod-1))+1+GrpDelPts:round(periodsamps/10):(periodsamps*startperiod)+GrpDelPts
            count=count+1;
            WindowedOutput=window.*c(WinStartPoint:WinStartPoint+ThreshPoints-1);
            if(rms(WindowedOutput))>0; % avoid log of zero errors
                
                 output=max(output,20*log10(rms(WindowedOutput)));
            end
        end
        %output(n)=output(n)/count
        %count
        
    else
        output=20*log10(rms(c));
    end;    

%figure(2);
    %plot(c(3*periodsamps:5*periodsamps)); %plot the output
    



    
