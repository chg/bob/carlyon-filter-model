%This is the hard bit and was written by Johannes Lyzenga
function x=johannes_ir1(fs,sz,f1,f2)

hz=round(sz/2);
f=(0:hz-1)/sz*fs;
% the order 2.5 lpf
a0=sqrt(1./(1+(f/f1).^(2*2.5)));
p  =-atan2(sqrt(2)*(f/f1),1+(i*f/f1).^2);
p0=-atan2(2*(f/f1)-(f/f1).^3,1-2*(f/f1).^2); p0=p0-2*pi*(p0>0);
p0=(p0+p)/2;
% the order 2 hpf
a=a0.*sqrt((1+(f/f2).^(2*2.0)));
p=atan2(sqrt(2)*(f/f2),1+(i*f/f2).^2);
p=p+p0;
% complete the spectra
a=[a 0 rot90(a(2:hz))'];
p=[p 0 -rot90(p(2:hz))'];
y=a.*(cos(p)+i*sin(p));
x=ifft(y); x=[x(hz+1:sz) x(1:hz)];
%figure(4), plot([real(x)' imag(x)'])
x=real(x);
